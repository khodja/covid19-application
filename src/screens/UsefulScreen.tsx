import React, { useEffect, useState } from 'react';
import { View, StyleSheet, ScrollView, ActivityIndicator } from 'react-native';
import global from '../../resources/global';
import Header from '../components/Header';
import UsefulBox from '../components/UsefulBox';
import ApiService from '../services/api';
const api = new ApiService();

const UsefulScreen = ({ navigation }: any): any => {
  const [articles, setArticles] = useState<any[]>([]);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    api.getUseful().then((fetched) => {
      setArticles(fetched);
      setLoading(false);
    });
  }, []);
  return (
    <>
      <View style={styles.container}>
        <Header navigation={navigation} />
        <ScrollView style={{ paddingVertical: 40 }}>
          {!loading ? (
            articles.map((article: any, key: number) => {
              return <UsefulBox key={key} navigation={navigation} article={article} />;
            })
          ) : (
            <ActivityIndicator size="large" color={global.colors.mainColor} />
          )}
          <View style={{ height: 60 }} />
        </ScrollView>
      </View>
    </>
  );
};

export default UsefulScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: global.colors.secondaryColor,
    position: 'relative',
  },
});
