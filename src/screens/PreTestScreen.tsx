import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  ActivityIndicator,
  Text,
  TouchableOpacity,
  AsyncStorage,
  ScrollView,
} from 'react-native';
import global from '../../resources/global';
import Header from '../components/Header';
import Icon from '../../assets/pretest.svg';
import { useFocusEffect } from '@react-navigation/native';

interface Props {
  navigation: any;
}
const PreTestScreen = ({ navigation }: Props): any => {
  const [loading, setLoading] = useState<boolean>(true);
  const [passed, setPassed] = useState<boolean>(true);
  const [desease, setDisease] = useState<number>(0);

  // useFocusEffect(
  //   React.useCallback(() => {
  //     getData();
  //   }, [setPassed])
  // );
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getData();
    });

    return unsubscribe;
  }, [navigation]);
  function setDataLoaded(data: any): void {
    let passed: boolean = false;
    let disease: number = 0;
    if (!data) disease = 0;
    else {
      disease = data;
      passed = true;
    }
    setLoading(false);
    setPassed(passed);
    setDisease(disease);
  }
  const getData = async () => {
    try {
      const passed: any = await AsyncStorage.getItem('passed');
      setDataLoaded(passed);
    } catch (e) {}
  };
  function getTitle(): string {
    if (!(passed && desease)) return 'Тест на COVID-19';
    if (desease > 4) return 'Подозрение на коронавирусную инфекцию';
    return 'У вас нет признаков коронавирусной инфекции';
  }
  function confirmText(): string {
    if (!(passed && desease)) return 'Начать тест';
    return 'Повторить тест';
  }
  function getText(): any {
    if (!(passed && desease)) {
      return (
        <Text style={styles.aboutText}>
          Этот тест проанализировав ваши ответы на вопросы определит, здоровы ли вы, или необходимо
          обследоваться у врача !
        </Text>
      );
    }
    if (desease > 4)
      return (
        <>
          <Text style={[styles.aboutText, { color: '#000', fontSize: 18, paddingBottom: 0 }]}>
            Ваши ответы указывают на симптомы коронавирусной инфекции
          </Text>
          <Text style={[styles.aboutText, { color: '#000', paddingBottom: 0, textAlign: 'left' }]}>
            Вы должны действовать следующим образом
          </Text>
          <Text style={[styles.aboutText, { paddingVertical: 0, textAlign: 'left' }]}>
            1. Оставайтесь дома
          </Text>
          <Text style={[styles.aboutText, { paddingVertical: 0, textAlign: 'left' }]}>
            2. Вызовите участкового врача (днем) или скорую медицинскую помощь - 103 (вечером)
          </Text>
          <Text style={[styles.aboutText, { paddingVertical: 0, textAlign: 'left' }]}>
            3. Пройдите лабораторный тест на определение коронавируса
          </Text>
          <Text style={[styles.aboutText, { paddingVertical: 0, textAlign: 'left' }]}>
            4. Все лица, контактировавшие с Вами, должны оставаться дома в течение двух недель
          </Text>
        </>
      );
    return (
      <>
        <Text style={[styles.aboutText, { color: '#000', fontSize: 18, paddingBottom: 0 }]}>
          Маловероятно, что Вы заражены коронавирусной инфекцией
        </Text>
        <Text style={[styles.aboutText, { color: '#000', paddingBottom: 0, textAlign: 'left' }]}>
          Действуйте следующим образом:
        </Text>
        <Text style={[styles.aboutText, { paddingVertical: 0, textAlign: 'left' }]}>
          1. Избегайте контактов с людьми и оставайтесь дома;
        </Text>
        <Text style={[styles.aboutText, { paddingVertical: 0, textAlign: 'left' }]}>
          2. Если Вы старше 60 лет и/или у Вас есть сахарный диабет, хронические заболевания легких,
          сердечно-сосудистой системы и других органов, следите за своим здоровьем и следуйте
          правилам личной гигиены;
        </Text>
        <Text style={[styles.aboutText, { paddingVertical: 0, textAlign: 'left' }]}>
          3. Если у Вас появятся симптомы болезни такие как лихорадка, сухой кашель, затрудненное
          дыхание вызовите участкового врача (днем) или скорую медицинскую помощь -103 (вечером).
        </Text>
      </>
    );
  }
  return (
    <>
      <View style={styles.container}>
        <Header navigation={navigation} />
        <ScrollView style={{ paddingTop: 40 }}>
          <View style={{ alignItems: 'center', position: 'relative', top: 30 }}>
            <Icon />
          </View>
          <View style={styles.infoBox}>
            {!loading ? (
              <>
                <Text style={styles.topText}>{getTitle()}</Text>
                {getText()}
              </>
            ) : (
              <ActivityIndicator size="large" color={global.colors.mainColor} />
            )}
          </View>
          {!loading ? (
            <View style={{ width: '92%', paddingHorizontal: 20, alignSelf: 'center' }}>
              <TouchableOpacity onPress={() => navigation.navigate('Test')} style={styles.button}>
                <Text style={styles.buttonText}>{confirmText()}</Text>
              </TouchableOpacity>
            </View>
          ) : null}
          <View style={{ height: 70 }} />
        </ScrollView>
      </View>
    </>
  );
};

export default PreTestScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: global.colors.secondaryColor,
    position: 'relative',
  },
  infoBox: {
    backgroundColor: 'white',
    width: `92%`,
    borderRadius: 20,
    marginBottom: 15,
    position: 'relative',
    padding: 20,
    overflow: 'hidden',
    alignSelf: 'center',
  },
  topText: {
    fontFamily: global.fonts.bold,
    fontSize: 20,
    textAlign: 'center',
  },
  aboutText: {
    fontFamily: global.fonts.regular,
    fontSize: 14,
    color: '#979797',
    textAlign: 'center',
    lineHeight: 24,
    paddingVertical: 15,
  },
  bottomText: {
    fontSize: 13,
    color: '#202020',
    textAlign: 'center',
  },
  button: {
    width: '100%',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    backgroundColor: global.colors.mainColor,
  },
  buttonText: {
    fontSize: 13,
    fontFamily: global.fonts.regular,
    color: 'white',
  },
});
