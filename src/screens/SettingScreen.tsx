import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  ActivityIndicator,
  AsyncStorage,
  ScrollView,
  Text,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import global from '../../resources/global';
import HeaderBack from '../components/HeaderBack';
import ApiService from '../services/api';

const api = new ApiService();
interface countryType {
  id: number;
  name: string;
}
const SettingScreen = ({ navigation }: any): any => {
  const [loading, setLoading] = useState<boolean>(true);
  const [search, setSearch] = useState<string>('');

  const [countries, setCountries] = useState<any[]>([]);
  const [fetched, setFetched] = useState<any[]>([]);
  const [currentId, setId] = useState<number>(0);
  useEffect(() => {
    fetchData();
    getData();
  }, []);
  function fetchData(): void {
    api.getCoutries().then((value) => {
      setLoading(false);
      setCountries(value);
      setFetched(value);
    });
  }
  function inputChange(inputText: string): void {
    let newList = [];
    setSearch(inputText);
    if (inputText !== '') {
      newList = countries.filter((item) => {
        const lc = item.name.toLowerCase();
        const filter = inputText.toLowerCase();
        return lc.includes(filter);
      });
    } else {
      newList = fetched;
    }
    setCountries(newList);
  }
  const getData = async () => {
    try {
      const getCountry: any = await AsyncStorage.getItem('country');
      const parsed: countryType = JSON.parse(getCountry);
      setId(parsed.id);
    } catch (e) {}
  };
  const storeData = async (data: any) => {
    try {
      await AsyncStorage.setItem('country', JSON.stringify(data));
    } catch (e) {}
  };
  function setCountry(id: number, name: string): void {
    storeData({ id: id, name: name });
    setId(id);
  }
  return (
    <>
      <View style={styles.container}>
        <HeaderBack navigation={navigation} />
        <ScrollView style={{ paddingTop: 40 }}>
          <View style={styles.infoBox}>
            <Text style={styles.topText}>Выбор региона</Text>
            {!loading ? (
              <>
                <TextInput
                  value={search}
                  style={[
                    styles.choice,
                    styles.choiceText,
                    { height: 50, borderColor: global.colors.mainColor },
                  ]}
                  placeholder={`Поиск`}
                  onChangeText={(text) => inputChange(text)}
                />

                {countries.map((country: countryType, key: number): any => {
                  return (
                    <TouchableOpacity
                      key={key}
                      onPress={() => setCountry(country.id, country.name)}
                      style={[
                        styles.choice,
                        country.id === currentId
                          ? { backgroundColor: global.colors.mainColor }
                          : {},
                      ]}>
                      <Text
                        style={[
                          styles.choiceText,
                          country.id === currentId ? { color: 'white' } : {},
                        ]}>
                        {country.name}
                      </Text>
                    </TouchableOpacity>
                  );
                })}
              </>
            ) : (
              <View style={{ paddingTop: 20 }}>
                <ActivityIndicator size="large" color={global.colors.mainColor} />
              </View>
            )}
          </View>
          <View style={{ height: 60 }} />
        </ScrollView>
      </View>
    </>
  );
};

export default SettingScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: global.colors.secondaryColor,
    position: 'relative',
  },
  infoBox: {
    backgroundColor: 'white',
    width: `92%`,
    borderRadius: 20,
    marginBottom: 15,
    position: 'relative',
    padding: 20,
    overflow: 'hidden',
    alignSelf: 'center',
  },
  topText: {
    fontFamily: global.fonts.bold,
    fontSize: 24,
    textAlign: 'center',
  },
  choice: {
    borderWidth: 1,
    borderColor: '#E2E2E2',
    borderRadius: 10,
    backgroundColor: 'white',
    marginTop: 15,
  },
  choiceText: {
    fontSize: 14,
    fontFamily: global.fonts.medium,
    padding: 15,
  },
});
