import React, { useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, AsyncStorage } from 'react-native';
import global from '../../resources/global';
import CustomStatusBar from '../components/CustomStatusBar';
import CustomIcon from '../../assets/preview.svg';
import PreviewFirst from '../../assets/preview1.svg';
import PreviewSecond from '../../assets/preview2.svg';
import PreviewThird from '../../assets/preview3.svg';

type ObjectsArray = {
  text: string;
};
const properties: number = global.strings.width - 40;

function PreviewScreen({ navigation }: any) {
  const storeData = async (data: any) => {
    try {
      await AsyncStorage.setItem('preview', JSON.stringify(data));
    } catch (e) {}
  };
  const [screen, setScreen] = useState<number>(0);
  const [currentText] = useState<ObjectsArray[]>([
    { text: 'Будьте в курсе всех обновлений из ВОЗ' },
    { text: 'Проверьте себя на симптомы COVID 19' },
    { text: 'Получайте только актуальные советы' },
  ]);
  const MiddleItem: React.SFC = (): any => {
    if (screen === 0) return <PreviewFirst width={`100%`} height={`100%`} />;
    if (screen === 1) return <PreviewSecond width={`100%`} height={`100%`} />;
    if (screen === 2) return <PreviewThird width={`100%`} height={`100%`} />;
  };
  function goToHome() {
    storeData(true);
    navigation.replace('Home');
  }
  return (
    <>
      <View style={styles.container}>
        <CustomStatusBar />
        <View style={[styles.absoluteBackground]}>
          <CustomIcon width={global.strings.width} height={global.strings.height + 100} />
        </View>
        <View style={styles.topInfo}>
          <View>
            <TouchableOpacity
              style={{
                backgroundColor: global.colors.secondaryColor,
                padding: 10,
                marginRight: 5,
                borderRadius: 5,
              }}
              onPress={goToHome}>
              <Text style={styles.topInfoText}>Пропустить</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.middle}>
          <View style={styles.middleItem}>
            <MiddleItem />
          </View>
        </View>
        <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={styles.middleText}>{currentText[screen].text}</Text>
        </View>
        <View
          style={{
            flex: 1,
            alignItems: 'flex-end',
            padding: 20,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View>
            {screen > 0 ? (
              <TouchableOpacity onPress={() => setScreen(screen - 1)} style={styles.button}>
                <Text style={styles.buttonText}>Назад</Text>
              </TouchableOpacity>
            ) : null}
          </View>
          <View>
            {screen < currentText.length - 1 ? (
              <TouchableOpacity onPress={() => setScreen(screen + 1)}>
                <View style={styles.button}>
                  <Text style={styles.buttonText}>Далее</Text>
                </View>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity onPress={goToHome}>
                <View style={[styles.button, { backgroundColor: '#00E369' }]}>
                  <Text style={[styles.buttonText, { color: 'white' }]}>Начать</Text>
                </View>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
    </>
  );
}

export default PreviewScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: global.colors.mainColor,
    position: 'relative',
  },
  middle: {
    flex: 5,
    alignItems: 'center',
    paddingHorizontal: 20,
    justifyContent: 'center',
  },
  middleItem: {
    width: properties,
    height: properties,
    overflow: 'hidden',
  },
  slide: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  topInfo: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  topInfoText: {
    color: global.colors.mainColor,
    fontSize: 18,
    fontFamily: 'Montserrat-Regular',
  },
  absoluteBackground: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: `100%`,
    height: `100%`,
  },
  middleText: {
    textAlign: 'center',
    color: 'white',
    paddingHorizontal: 10,
    fontSize: 24,
    maxWidth: 310,
    fontFamily: global.fonts.bold,
  },
  button: {
    backgroundColor: 'white',
    borderRadius: 20,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    width: 75,
    shadowColor: 'rgba(0,0,0,.15)',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  buttonText: {
    color: global.colors.mainColor,
    fontSize: 14,
    fontFamily: global.fonts.medium,
  },
});
