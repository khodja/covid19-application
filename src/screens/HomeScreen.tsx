import React, { useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  AsyncStorage,
} from 'react-native';

import global from '../../resources/global';
import Header from '../components/Header';
import InfoBox from '../components/InfoBox';
import ApiService from '../services/api';
import Refresh from '../../assets/refresh.svg';
import Health from '../../assets/healthy.svg';
import Added from '../../assets/added.svg';
import Earth from '../../assets/earth.svg';
import { AdMobBanner } from 'expo-ads-admob';

const api = new ApiService();

const HomeScreen = ({ navigation }: any): any => {
  const [loading, setLoading] = useState<boolean>(true);
  const [data, setData] = useState<any>([]);
  const [world, setWorld] = useState<any>([]);
  const [country, setCountry] = useState<string>('');
  useEffect(() => {
    const onFocusChanges = navigation.addListener('focus', () => {
      getData();
    });

    return onFocusChanges;
  }, [navigation]);

  function fetchData(id: number, country: string): void {
    api.getCorona(id).then((value) => {
      setData(value.data);
      setWorld(value.world);
      setLoading(false);
      setCountry(country);
    });
  }
  const getData = async () => {
    try {
      const localCountry: any = await AsyncStorage.getItem('country');
      if (localCountry) {
        const parsed = JSON.parse(localCountry);
        if (parsed.name !== country) {
          fetchData(parsed.id, parsed.name);
        } else {
          setLoading(false);
        }
      } else {
        fetchData(1, 'Afghanistan');
      }
    } catch (e) {}
  };
  function reloadInfo(): void {
    setLoading(true);
    getData();
  }
  interface Stats {
    country: string;
    percentage: number;
    quantity: number;
    type: number;
  }
  function getPercentage(confirmed: number, healed: number): number {
    const percentage: number = healed / (confirmed / 100);
    return Math.round(percentage);
  }
  function getAdded(yesterday: number, today: number): number {
    return today > yesterday ? today - yesterday : today - yesterday;
  }
  const BottomBlock = ({ country, percentage, quantity, type }: Stats) => {
    return (
      <>
        <View>
          <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
            <View
              style={{
                padding: 5,
                borderRadius: 10,
                backgroundColor: global.colors.mainColor,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: 'white',
                  fontSize: 24,
                  paddingRight: 10,
                  fontFamily: global.fonts.regular,
                }}>
                {country}
              </Text>
              <Earth />
            </View>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingBottom: 20,
          }}>
          <View
            style={{
              flex: 1,
              backgroundColor: !type ? global.colors.mainColor : 'white',
              borderWidth: 1,
              borderColor: global.colors.mainColor,
              padding: 10,
              borderRadius: 20,
              maxWidth: '49%',
            }}>
            <Health />
            <Text
              style={{
                color: !type ? 'white' : global.colors.mainColor,
                fontSize: 16,
                paddingVertical: 10,
                fontFamily: global.fonts.medium,
              }}>
              Вылечились
            </Text>
            <Text
              style={{
                color: !type ? 'white' : global.colors.mainColor,
                fontSize: 24,
                fontFamily: global.fonts.bold,
              }}>
              {percentage}%
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              backgroundColor: !type ? 'white' : global.colors.mainColor,
              borderColor: global.colors.mainColor,
              borderWidth: 1,
              borderRadius: 20,
              padding: 10,
              maxWidth: '49%',
            }}>
            <Added />
            <Text
              style={{
                color: type ? 'white' : global.colors.mainColor,
                fontSize: 16,
                paddingVertical: 10,
                fontFamily: global.fonts.medium,
              }}>
              На сегодня
            </Text>
            <Text
              style={{
                color: type ? 'white' : global.colors.mainColor,
                fontSize: 24,
                fontFamily: global.fonts.bold,
              }}>
              +{quantity}
            </Text>
          </View>
        </View>
      </>
    );
  };
  return (
    <>
      <View style={styles.container}>
        <Header navigation={navigation} />
        <ScrollView style={{ paddingTop: 40 }}>
          {!loading ? (
            <>
              <View
                style={{
                  width: '92%',
                  alignSelf: 'center',
                  padding: 10,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  marginBottom: 20,
                  backgroundColor: 'white',
                  borderRadius: 15,
                }}>
                <View>
                  <Text
                    style={{
                      color: global.colors.mainColor,
                      fontSize: 14,
                      fontFamily: global.fonts.bold,
                    }}>
                    Обновление от {data[0].date.replace(/\-/g, '.')}
                  </Text>
                </View>
                <View>
                  <TouchableOpacity
                    onPress={() => reloadInfo()}
                    style={{
                      backgroundColor: global.colors.mainColor,
                      height: 40,
                      width: 40,
                      borderRadius: 40,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Refresh color={`white`} />
                  </TouchableOpacity>
                </View>
              </View>
              <View
                style={{
                  width: '92%',
                  alignSelf: 'center',
                  padding: 10,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  marginBottom: 20,
                  backgroundColor: global.colors.mainColor,
                  borderRadius: 15,
                }}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: 14,
                    fontFamily: global.fonts.bold,
                  }}>
                  Регион - {country}
                </Text>
                <TouchableOpacity
                  onPress={() => navigation.navigate('Setting')}
                  style={{
                    borderColor: global.colors.mainColor,
                    borderWidth: 1,
                    backgroundColor: 'white',
                    borderRadius: 5,
                    height: 40,
                    padding: 10,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      color: global.colors.mainColor,
                      fontSize: 14,
                      fontFamily: global.fonts.bold,
                    }}>
                    Выбрать
                  </Text>
                </TouchableOpacity>
              </View>
              <InfoBox
                navigation={navigation}
                innerType={1}
                data={data}
                count={data[0].recovered}
                country={country}
              />
              <InfoBox
                navigation={navigation}
                innerType={2}
                data={data}
                count={data[0].confirmed}
                country={country}
              />
              <InfoBox
                navigation={navigation}
                innerType={3}
                data={data}
                count={data[0].deaths}
                country={country}
              />
              <View>
                <AdMobBanner
                  bannerSize="fullBanner"
                  style={{ marginBottom: 20 }}
                  adUnitID={'ca-app-pub-8299324069750466/7271577368'}
                />
              </View>

              <View
                style={{
                  width: '92%',
                  alignSelf: 'center',
                  marginBottom: 20,
                }}>
                <Text
                  style={{
                    color: global.colors.mainColor,
                    fontSize: 24,
                    fontFamily: global.fonts.bold,
                    paddingBottom: 15,
                  }}>
                  Статистика
                </Text>
                <BottomBlock
                  type={0}
                  country={`Весь мир`}
                  percentage={getPercentage(Number(world.confirmed), Number(world.recovered))}
                  quantity={Math.abs(world.added)}
                />
                <BottomBlock
                  type={1}
                  country={country}
                  percentage={getPercentage(Number(data[0].confirmed), Number(data[0].recovered))}
                  quantity={getAdded(Number(data[1].recovered), Number(data[0].recovered))}
                />
              </View>
            </>
          ) : (
            <ActivityIndicator size="large" color={global.colors.mainColor} />
          )}
          <View style={{ height: 40 }} />
        </ScrollView>
      </View>
    </>
  );
};

export default HomeScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: global.colors.secondaryColor,
    position: 'relative',
  },
});
