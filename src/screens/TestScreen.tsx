import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  AsyncStorage,
  Text,
  Alert,
} from 'react-native';
import global from '../../resources/global';
import HeaderBack from '../components/HeaderBack';
import QuestionBox from '../components/QuestionBox';

const TestScreen = ({ navigation }: any): any => {
  const [choice, setChoice] = useState<number>(0);
  const [filled, setFiller] = useState<boolean>(false);
  function addChoice(value: number): void {
    setChoice(choice + value);
  }
  function saveTest(): void {
    if (filled) {
      storeData(choice);
      navigation.navigate('Check');
    } else {
      Alert.alert('Заполните возраст');
    }
  }
  const storeData = async (data: any) => {
    try {
      await AsyncStorage.setItem('passed', JSON.stringify(data));
    } catch (e) {}
  };
  return (
    <>
      <View style={styles.container}>
        <HeaderBack navigation={navigation} />
        <ScrollView style={{ paddingTop: 40 }}>
          <QuestionBox setChoice={addChoice} setFiller={setFiller} type={`input`} />
          <QuestionBox
            setChoice={addChoice}
            description={`Есть ли у Вас какие-либо из следующих хронических заболеваний? (сахарный диабет, хронические заболевания легких, сердечно-сосудистые болезни и др.)`}
          />
          <QuestionBox
            setChoice={addChoice}
            description={`Был ли у вас контакт с кем-либо, у кого диагностирован коронавирус?`}
          />
          <QuestionBox
            setChoice={addChoice}
            description={`Были ли Вы за рубежом в течение последних двух недель?`}
          />
          <QuestionBox
            setChoice={addChoice}
            description={`Присутствуют ли у Вас повышение температуры тела?`}
          />
          <QuestionBox
            setChoice={addChoice}
            description={`Присутствуют ли у Вас затрудненное дыхание?`}
          />
          <QuestionBox setChoice={addChoice} description={`Присутствуют ли у Вас сухой кашель?`} />
          <View style={{ width: '92%', paddingHorizontal: 20, alignSelf: 'center' }}>
            <TouchableOpacity onPress={saveTest} style={[styles.button]}>
              <Text style={styles.buttonText}>Проверить</Text>
            </TouchableOpacity>
          </View>
          <View style={{ height: 100 }} />
        </ScrollView>
      </View>
    </>
  );
};

export default TestScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: global.colors.secondaryColor,
    position: 'relative',
  },
  button: {
    width: '100%',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    backgroundColor: global.colors.mainColor,
  },
  buttonText: {
    fontSize: 13,
    fontFamily: global.fonts.regular,
    color: 'white',
  },
});
