import React, { useState, useEffect } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity, Text } from 'react-native';
import { LineChart, XAxis, YAxis } from 'react-native-svg-charts';
import * as shape from 'd3-shape';
import { AdMobBanner } from 'expo-ads-admob';
import global from '../../resources/global';
import HeaderBack from '../components/HeaderBack';

interface Props {
  navigation: any;
  route: any;
}
const StatScreen = ({ navigation, route }: Props): any => {
  const { type, stats } = route.params;
  const [data, setData] = useState<any[]>([]);
  useEffect(() => {
    filterData();
  }, []);
  const axisData = stats.slice(0, 9);
  function filterData(): void {
    let filteredData: any = [];
    axisData.forEach((item: any) => {
      if (type === 1) filteredData.push(Number(item.recovered));
      if (type === 2) filteredData.push(Number(item.confirmed));
      if (type === 3) filteredData.push(Number(item.deaths));
    });
    setData(filteredData.reverse());
  }
  function totalCount(): number {
    let count: number = 0;
    if (type === 1) count = Number(stats[0].recovered);
    if (type === 2) count = Number(stats[0].confirmed);
    if (type === 3) count = Number(stats[0].deaths);
    return count;
  }
  const axesSvg: object = { fontSize: 12, fill: 'rgba(0,0,0,.4)' };
  const verticalContentInset: object = { top: 30, bottom: 10 };
  const xAxisHeight: number = 20;
  function getColor() {
    if (type === 1) return '#2AD26D';
    if (type === 2) return '#F1C247';
    if (type === 3) return '#FFACAC';
  }
  function getText() {
    if (type === 1) return 'Выздоровело';
    if (type === 2) return 'Подтвержденные';
    if (type === 3) return 'Летальные исходы';
  }
  function getCountByDate(stat: any): number {
    let count: number = 0;
    if (type === 1) count = Number(stat.recovered);
    if (type === 2) count = Number(stat.confirmed);
    if (type === 3) count = Number(stat.deaths);
    return count;
  }
  return (
    <>
      <View style={styles.container}>
        <HeaderBack navigation={navigation} />
        <ScrollView style={{ paddingTop: 40 }}>
          <View style={styles.infoBox}>
            <Text style={styles.topText}>{getText()}</Text>
            <View
              style={{
                flexDirection: 'row',
                paddingTop: 20,
                alignItems: 'center',
              }}>
              <Text style={[styles.middleText]}>Всего: </Text>
              <Text style={[styles.middleText, { color: getColor() }]}> {totalCount()}</Text>
            </View>
            <View style={{ minHeight: 280, flexDirection: 'row' }}>
              <YAxis
                data={data}
                style={{ marginBottom: xAxisHeight }}
                contentInset={verticalContentInset}
                svg={axesSvg}
              />
              <View style={{ flex: 1, marginLeft: 10 }}>
                <LineChart
                  style={{ flex: 1 }}
                  data={data}
                  contentInset={verticalContentInset}
                  curve={shape.curveNatural}
                  svg={{ stroke: getColor(), strokeWidth: 2 }}
                />
                <XAxis
                  style={{
                    marginHorizontal: -10,
                    paddingTop: 5,
                    borderTopWidth: 1,
                    borderTopColor: 'rgba(0,0,0,.2)',
                    height: xAxisHeight,
                  }}
                  data={data}
                  contentInset={{ left: 10, right: 10 }}
                  svg={axesSvg}
                />
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingTop: 10,
              }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={[styles.middleText]}>От: </Text>
                <Text style={[styles.middleText, { color: getColor() }]}>
                  {' ' + stats[stats.length - 1].date.replace(/\-/g, '.')}
                </Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Text style={[styles.middleText]}>До: </Text>
                <Text style={[styles.middleText, { color: getColor() }]}>
                  {' ' + stats[0].date.replace(/\-/g, '.')}
                </Text>
              </View>
            </View>
          </View>
          <View>
            <AdMobBanner
              bannerSize="fullBanner"
              style={{ marginBottom: 20 }}
              adUnitID={'ca-app-pub-8299324069750466/2286596578'}
            />
          </View>
          <Text style={styles.detailText}>Детальная информация по датам</Text>
          {stats.map((stat: any, key: number) => {
            if (getCountByDate(stat) !== 0) {
              return (
                <View key={key}>
                  <View style={[styles.dateBox, { backgroundColor: 'white' }]}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                      }}>
                      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={[styles.middleText, { color: 'rgba(101, 87, 176, 1)' }]}>
                          {`${getText()}: `}
                        </Text>
                        <Text style={[styles.middleText, { color: getColor() }]}>
                          {getCountByDate(stat)}
                        </Text>
                      </View>
                      <View>
                        <Text
                          style={[
                            styles.dateText,
                            { color: 'rgba(101, 87, 176, 1)', textAlign: 'right' },
                          ]}>
                          На дату {stat.date.replace(/\-/g, '.')}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              );
            }
          })}
          <View style={{ height: 100 }} />
        </ScrollView>
      </View>
    </>
  );
};

export default StatScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: global.colors.secondaryColor,
    position: 'relative',
  },
  button: {
    width: '100%',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    backgroundColor: global.colors.mainColor,
  },
  buttonText: {
    fontSize: 13,
    fontFamily: global.fonts.regular,
    color: 'white',
  },

  infoBox: {
    backgroundColor: 'white',
    width: `92%`,
    borderRadius: 20,
    marginBottom: 15,
    position: 'relative',
    padding: 20,
    overflow: 'hidden',
    alignSelf: 'center',
  },
  topText: {
    fontFamily: global.fonts.bold,
    fontSize: 24,
    textAlign: 'center',
  },
  middleText: {
    fontFamily: global.fonts.bold,
    fontSize: 13,
  },
  detailText: {
    fontFamily: global.fonts.bold,
    fontSize: 18,
    width: '92%',
    alignSelf: 'center',
    marginBottom: 20,
    padding: 8,
    color: 'white',
    backgroundColor: global.colors.mainColor,
    borderRadius: 10,
  },
  dateBox: {
    width: `92%`,
    borderRadius: 10,
    marginBottom: 15,
    position: 'relative',
    padding: 15,
    overflow: 'hidden',
    alignSelf: 'center',
  },
  dateText: {
    fontFamily: global.fonts.regular,
    fontSize: 13,
  },
});
