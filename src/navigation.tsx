import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import HomeScreen from './screens/HomeScreen';
import PreTestScreen from './screens/PreTestScreen';
import TestScreen from './screens/TestScreen';
import PreviewScreen from './screens/PreviewScreen';
import UsefulScreen from './screens/UsefulScreen';
import UsefulInfoScreen from './screens/UsefulInfoScreen';
import SettingScreen from './screens/SettingScreen';
import StatScreen from './screens/StatScreen';
import global from '../resources/global';
import Home from '../assets/home.svg';
import Check from '../assets/check.svg';
import Useful from '../assets/useful.svg';
import { AsyncStorage } from 'react-native';

const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();
const AppContainer = () => {
  const [preview, setPreview] = useState<boolean>(true);
  useEffect(() => {
    setPreviewData();
  }, []);
  const setPreviewData = async () => {
    try {
      const preview: any = await AsyncStorage.getItem('preview');
      if (JSON.parse(preview)) setPreview(false);
    } catch (e) {}
  };
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={preview ? `Preview` : `Home`}>
        <Stack.Screen name="Preview" component={PreviewScreen} options={{ headerShown: false }} />
        <Stack.Screen name="Home" options={{ headerShown: false }}>
          {(navigation) => {
            return (
              <Tab.Navigator
                barStyle={{
                  height: 70,
                  justifyContent: 'center',
                  backgroundColor: 'white',
                  borderTopRightRadius: 20,
                  borderTopLeftRadius: 20,
                }}
                backBehavior={'none'}
                activeColor={global.colors.mainColor}
                inactiveColor={`#E2E2E2`}>
                <Tab.Screen
                  options={{
                    tabBarLabel: 'Статистика',
                    tabBarIcon: ({ color }) => <Home width={24} height={24} color={color} />,
                  }}
                  name="Home"
                  component={HomeScreen}
                />
                <Tab.Screen
                  options={{
                    tabBarLabel: 'Проверка',
                    tabBarIcon: ({ color }) => <Check width={24} height={24} color={color} />,
                  }}
                  name="Check"
                  component={PreTestScreen}
                />
                <Tab.Screen
                  options={{
                    tabBarLabel: 'Полезное',
                    tabBarIcon: ({ color }) => <Useful width={24} height={24} color={color} />,
                  }}
                  name="Useful"
                  component={UsefulScreen}
                />
              </Tab.Navigator>
            );
          }}
        </Stack.Screen>
        <Stack.Screen name="Test" component={TestScreen} options={{ headerShown: false }} />
        <Stack.Screen name="Setting" component={SettingScreen} options={{ headerShown: false }} />
        <Stack.Screen name="Stats" component={StatScreen} options={{ headerShown: false }} />
        <Stack.Screen
          name="UsefulInfo"
          component={UsefulInfoScreen}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppContainer;
