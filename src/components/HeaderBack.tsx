import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import global from '../../resources/global';
import Background from '../../assets/background.svg';
import Back from '../../assets/arrow.svg';
import CustomStatusBar from './CustomStatusBar';

interface Props {
  navigation: any;
}
const HeaderBack = ({ navigation }: Props) => {
  return (
    <>
      <CustomStatusBar />
      <View style={[styles.absoluteBackground]}>
        <Background
          color={global.colors.mainColor}
          width={global.strings.width + 5}
          height={global.strings.width}
        />
      </View>
      <View style={styles.header}>
        <View>
          <TouchableOpacity onPress={() => navigation.goBack()} style={{ padding: 5 }}>
            <Back width={30} height={30} />
          </TouchableOpacity>
        </View>
        <Text style={styles.title}>{global.strings.name}</Text>
      </View>
    </>
  );
};

export default HeaderBack;

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
  },
  title: {
    color: 'white',
    fontFamily: global.fonts.bold,
    fontSize: 24,
  },
  absoluteBackground: {
    position: 'absolute',
    right: 0,
    top: -global.strings.width / 3,
  },
});
