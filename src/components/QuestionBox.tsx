import React , { useState , useEffect } from 'react'
import { View, Text ,StyleSheet, TextInput , TouchableOpacity, Alert } from 'react-native'
import global from '../../resources/global';

interface Props {
  description?: string,
  type?:string,
  setChoice(value:number): void,
  setFiller?:any
}
const QuestionBox = ({ type , description , setChoice , setFiller}: Props) => {
  if(type === 'input'){
    const [input , changeInput] = useState('');
    const inputHandler = (text:string):void => {
      if(text.length <= 2){
      let newText = '';
      let numbers = '0123456789';
      for (var i=0; i < text.length; i++) {
          if(numbers.indexOf(text[i]) > -1 ) {
              newText = newText + text[i];
          }
      }
      changeInput(newText);
      }
  }

    useEffect(() => {
      const checkTest = input.length ? true : false;
      setFiller(checkTest);
    }, [input])
    return (
      <View style={styles.infoBox}>
        <Text style={styles.topText}>Тест на COVID-19</Text>
        <Text style={styles.aboutText}>Введите ваш возраст</Text>
        <TextInput keyboardType={`numeric`} value={input} onChangeText={text => inputHandler(text)} style={styles.input}/>
      </View>
    )
  }
  const [chosen, setChosen] = useState(-1);
  function makeChoice(value: number) : void{
    if(value !== chosen){
      setChosen(value);
      setChoice(value);
    }
  }
  return (
    <View style={styles.infoBox}>
      <Text style={styles.aboutText}>{description}</Text>
      <View style={styles.buttonWrap}>
        <TouchableOpacity onPress={() => makeChoice(1)} style={[styles.button,(chosen === 1 ? {backgroundColor:global.colors.mainColor}: {})]}>
          <Text style={[styles.buttonText, (chosen === 1 ? {color:'white'}: {})]}>Да</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => makeChoice(-1)} style={[styles.button,(chosen === -1 ? {backgroundColor:global.colors.mainColor}: {})]}>
          <Text style={[styles.buttonText, (chosen === -1 ? {color:'white'}: {})]}>Нет</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default QuestionBox;
const styles = StyleSheet.create({
  input:{
    fontSize:14,
    padding: 10,
    borderWidth:1,
    borderColor: '#E2E2E2',
    borderRadius:10
  },
  infoBox:{
    backgroundColor:'white',
    width: `92%`,
    borderRadius:20,
    marginBottom:15,
    position: 'relative',
    padding: 20,
    overflow: 'hidden',
    alignSelf:'center'
  },
  topText:{
    fontFamily: global.fonts.bold,
    fontSize:24,
    textAlign:'center'
  },
  aboutText:{
    fontFamily: global.fonts.regular,
    fontSize: 13,
    color: '#202020',
    textAlign:'center',
    lineHeight:24,
    paddingVertical:15
  },
  buttonWrap:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between'
  },
  button:{
    width: '48%',
    height:40,
    alignItems:'center',
    justifyContent:'center',
    borderRadius: 10,
    borderWidth:1,
    borderColor: global.colors.mainColor
  },
  buttonText:{
    fontSize:13,
    fontFamily: global.fonts.regular,
    color: global.colors.mainColor
  }
});
