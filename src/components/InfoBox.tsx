import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import global from '../../resources/global';
import TypeFirst from '../../assets/zdorov.svg';
import TypeSecond from '../../assets/confirmed.svg';
import TypeThird from '../../assets/umer.svg';
import InfoFirst from '../../assets/zdorov1.svg';
import InfoSecond from '../../assets/confirmed1.svg';
import InfoThird from '../../assets/umer1.svg';

interface Props {
  innerType: number;
  country: string;
  count: number;
  navigation: any;
  data: any;
}
const InfoBox = ({ innerType, country, count, navigation, data }: Props) => {
  const ImageBottom = (): any => {
    if (innerType === 1) return <TypeFirst />;
    if (innerType === 2) return <TypeSecond />;
    if (innerType === 3) return <TypeThird />;
  };
  const InfoIcon = (): any => {
    if (innerType === 1) return <InfoFirst />;
    if (innerType === 2) return <InfoSecond />;
    if (innerType === 3) return <InfoThird />;
  };
  const TextCase = (): any => {
    if (innerType === 1) return 'Выздоровело';
    if (innerType === 2) return 'Подтвержденные случаи';
    if (innerType === 3) return 'Летальные исходы';
  };
  const Hashtag = (): any => {
    if (innerType === 1) return '#спасибо_врачам';
    if (innerType === 2) return '#остовайтесь_дома';
    if (innerType === 3) return 'Скорбим';
  };
  function getColor() {
    if (innerType === 1) return '#2AD26D';
    if (innerType === 2) return '#F1C247';
    if (innerType === 3) return '#FFACAC';
  }
  return (
    <View style={styles.box}>
      <View style={styles.absolute}>
        <ImageBottom />
      </View>
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
        <Text style={styles.count}>
          {count} <Text style={{ fontSize: 16, color: 'rgba(0,0,0,.7)' }}>людей</Text>
        </Text>
        <View style={styles.country}>
          <Text style={styles.countryText}>{country}</Text>
        </View>
      </View>
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
        <Text style={styles.type}>
          <TextCase />
        </Text>
        <Text style={[styles.type, { color: getColor() }]}>
          <Hashtag />
        </Text>
      </View>
      <View style={{ alignItems: 'flex-end' }}>
        <InfoIcon />
      </View>
      <View style={{ alignItems: 'flex-end', paddingTop: 15 }}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Stats', { type: innerType, stats: data })}>
          <Text style={styles.buttonText}>Подробнее</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default InfoBox;
const styles = StyleSheet.create({
  box: {
    backgroundColor: 'white',
    width: `92%`,
    height: 200,
    borderRadius: 20,
    marginBottom: 15,
    position: 'relative',
    padding: 20,
    overflow: 'hidden',
    alignSelf: 'center',
  },
  type: {
    fontFamily: global.fonts.regular,
    fontSize: 12,
    lineHeight: 24,
  },
  absolute: {
    position: 'absolute',
    bottom: -10,
    left: -10,
  },
  count: {
    color: 'black',
    fontSize: 24,
    fontFamily: global.fonts.bold,
  },
  country: {
    backgroundColor: '#C4C4C4',
    justifyContent: 'center',
    borderRadius: 5,
    padding: 3,
  },
  countryText: {
    color: 'white',
    fontFamily: global.fonts.bold,
    fontSize: 14,
    textTransform: 'uppercase',
  },
  button: {
    width: 140,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    backgroundColor: global.colors.mainColor,
  },
  buttonText: {
    fontSize: 13,
    fontFamily: global.fonts.regular,
    color: 'white',
  },
});
