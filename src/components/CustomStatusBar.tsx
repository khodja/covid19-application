import React from 'react';
import { View, StatusBar } from 'react-native';
import Constants from 'expo-constants';
import global from '../../resources/global';

const CustomStatusBar = () => {
  return (
    <View
      style={{
        backgroundColor: global.colors.mainColor,
        height: Constants.statusBarHeight,
      }}>
      <StatusBar backgroundColor={global.colors.mainColor} barStyle="light-content" />
    </View>
  );
};

export default CustomStatusBar;
