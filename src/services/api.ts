class ApiService {
  _host = 'http://demo.indev.uz/';
  _apiBase = this._host + '/api/';
  _headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };

  async getResources(url: string) {
    const response = await fetch(this._apiBase + url);
    if (!response.ok) {
      throw new Error(`Could not fetch ${url}, received ${response}`);
    }
    return await response.json();
  }

  getCorona(id: number) {
    return this.getResources(`corona/info/${id}`);
  }
  getCoutries() {
    return this.getResources(`corona/countries`);
  }
  getUseful() {
    return this.getResources(`corona/useful`);
  }
  getUsefulById(id: number) {
    return this.getResources(`corona/useful/${id}`);
  }
}
export default ApiService;
