import {items, item, user, categories, settings, searchItems} from "../constants/actionTypes";

const initialState = {
    items:{
        items: [],
        loading: true,
        error: null,
    },
    searchItems:{
        items: [],
        loading: true,
        error: null,
    },
    item: {
        item: null,
        loading: true,
        error: null,
    },
    categories:{
        items: [],
        loading: false,
        error: null,
    },
    auth: {
        token: null,
        loading: false,
        error: null,
    },
    user: {
        item: null,
        loading: false,
        error: null,
    },
    theme: "light",
    lang: "uz",
    fontSize: 12,
    search: null,
    articleStyle: 2,
};

const reducer = ( state = initialState, action) => {

    switch (action.type) {
        case items.REQUEST:
            return {
                ...state,
                items:{
                    items: action.page===1?[]:state.items.items,
                    loading: true,
                    error: null,
                }
            };
        case items.SUCCESS:
            return {
                ...state,
                items:{
                    items: [...state.items.items,...action.payload],
                    loading: false,
                    error: null,
                }
            };
        case items.FAILURE:
            return {
                ...state,
                items:{
                    items: action.page===1?[]:state.items.items,
                    loading: false,
                    error: action.payload,
                }
            };
        case categories.REQUEST:
            return {
                ...state,
                categories:{
                    items: [],
                    loading: true,
                    error: null,
                }
            };
        case categories.SUCCESS:
            return {
                ...state,
                categories:{
                    items: action.payload,
                    loading: false,
                    error: null,
                }
            };
        case categories.FAILURE:
            return {
                ...state,
                categories:{
                    items: [],
                    loading: false,
                    error: action.payload,
                }
            };
        case searchItems.REQUEST:
            return {
                ...state,
                searchItems:{
                    items: [],
                    loading: true,
                    error: null,
                }
            };
        case searchItems.SUCCESS:
            return {
                ...state,
                searchItems:{
                    items: action.payload,
                    loading: false,
                    error: null,
                }
            };
        case searchItems.FAILURE:
            return {
                ...state,
                searchItems:{
                    items: [],
                    loading: false,
                    error: action.payload,
                }
            };
        case item.REQUEST:
            return {
                ...state,
                item: {
                    item: null,
                    loading: true,
                    error: null,
                },
            };
        case item.SUCCESS:
            return {
                ...state,
                item: {
                    item: action.payload,
                    loading: false,
                    error: null,
                },
            };
        case item.FAILURE:
            return {
                ...state,
                item: {
                    item: null,
                    loading: false,
                    error: action.payload,
                },
            };
        case settings.THEME_CHANGE:
            return {
                ...state,
                theme: action.payload,
            };
        case settings.LANG_CHANGE:
            return {
                ...state,
                lang: action.payload,
            };
        case settings.FONTSIZE_CHANGE:
            return {
                ...state,
                fontSize: action.payload,
            };
        case settings.SEARCH_CHANGE:
            return {
                ...state,
                search: action.payload,
            };
        case settings.ARTICLE_STYLE_CHANGE:
            return {
                ...state,
                articleStyle: action.payload,
            };
        case user.LOGIN_REQUEST:
            return {
                ...state,
                auth:{
                    token: null,
                    loading: true,
                    error: null,
                }
            };
        case user.LOGIN_SUCCESS:
            return {
                ...state,
                auth:{
                    token: action.payload,
                    loading: false,
                    error: null,
                }
            };
        case user.LOGOUT:
            return {
                ...state,
                auth: initialState.auth,
                user: initialState.user,
            };
        case user.REGISTER_FAILURE:
            return {
                ...state,
                auth:{
                    token: null,
                    loading: false,
                    error: action.payload,
                }
            };
        case user.REGISTER_REQUEST:
            return {
                ...state,
                auth:{
                    token: null,
                    loading: true,
                    error: null,
                }
            };
        case user.REGISTER_SUCCESS:
            return {
                ...state,
                auth:{
                    token: action.payload,
                    loading: false,
                    error: null,
                }
            };
        case user.PROFILE_FAILURE:
            return {
                ...state,
                user:{
                    item: null,
                    loading: false,
                    error: action.payload,
                }
            };
        case user.PROFILE_REQUEST:
            return {
                ...state,
                user:{
                    item: null,
                    loading: true,
                    error: null,
                }
            };
        case user.PROFILE_SUCCESS:
            return {
                ...state,
                user:{
                    item: action.payload,
                    loading: false,
                    error: null,
                }
            };
        case user.LOGIN_FAILURE:
            return {
                ...state,
                auth:{
                    token: null,
                    loading: false,
                    error: action.payload,
                }
            };

        default: return state;
    }
};

export default reducer;
