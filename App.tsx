import React, { useEffect, useState } from 'react';
import AppContainer from './src/navigation';
import * as Font from 'expo-font';

export default function App() {
  const [fontLoaded, setFontLoaded] = useState<boolean>(false);

  useEffect(() => {
    Font.loadAsync({
      'Montserrat-Regular': require('./assets/fonts/Montserrat-Regular.ttf'),
      'Montserrat-Medium': require('./assets/fonts/Montserrat-Medium.ttf'),
      'Montserrat-Bold': require('./assets/fonts/Montserrat-Bold.ttf'),
    }).then(() => {
      setFontLoaded(true);
    });
  }, []);

  return fontLoaded ? <AppContainer /> : null;
}
