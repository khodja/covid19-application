/* eslint-disable global-require */
const images = {
  reviewImage: require('../assets/preview.png'),
  image: require('../assets/image.png'),
};

export default images;
