const colors = {
  mainColor: '#25265E',
  secondaryColor: '#F0F3FD',
};

export default colors;
