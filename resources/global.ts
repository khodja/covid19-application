import images from './images';
import colors from './colors';
import fonts from './fonts';
import palettes from './palettes';
import strings from './strings';

const global = {
  images,
  colors,
  fonts,
  palettes,
  strings,
};

export default global;
