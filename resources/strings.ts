import { Dimensions } from 'react-native';

const { height, width } = Dimensions.get('window');
const strings = { width, height, name: '#StayHome' };

export default strings;
